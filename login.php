<?php
include 'header.php';
?>
<div class="container">
    <div class="row mt-3">
        <div class="col-md-4 offset-md-4 col-lg-4 offset-lg-4">
            <div class="card">
                <div class="card-header">Form Login</div>
                <div class="card-body">
                    <form action="p-login.php" method="post">
                    <div class="form-group">
                        <label>Username</label>
                        <input type="text" class="form-control" id="username" name="username" aria-describedby="emailHelp" placeholder="Enter username">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary mt-2">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'footer.php';
?>