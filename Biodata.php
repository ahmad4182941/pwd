<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Biodata </title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css">
</head>
<body class="biru">
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
          <a class="navbar-brand" href="#">Navbar</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="index.html">Home</a>
              </li>
              <li class="nav-item"> 
                <a class="nav-link" href="biodata.html">Biodata</a>
              </li>
              <li class="nav-item"> 
                <a class="nav-link" href="berita.html">Berita</a>
              </li>
              <li class="nav-item"> 
                <a class="nav-link" href="galeri.html">Galeri</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Dropdown 
                </a>
                <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="#">Action</a></li>
                  <li><a class="dropdown-item" href="#">Another action</a></li>
                  <li><hr class="dropdown-divider"></li>
                  <li><a class="dropdown-item" href="#">Something else here</a></li>
                </ul>
              </li>
              <li class="nav-item">
                <a class="nav-link disabled" aria-disabled="true">Disabled</a>
              </li>
            </ul>
            <form class="d-flex" role="search">
              <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
              <button class="btn btn-outline-success" type="submit">Search</button>
            </form>
          </div>
        </div>
      </nav>
    <div class="row">
        <div class="col-md-3 p-3">
            <div class="card">
                <div class="card-body">
                    <img src= "001.jpg" class="img-fluid" alt="Foto diri sendiri">   
                </div>
                
            </div>
            Ahmad Miftah Fadhli  
        </div>
        <div class="col-md-9 p-3">
            <div class="card">
                <div class="card-body bg-info">
                    <div class="table-responsive">
                    </div>
                    <table class="table table-hover table-warning">
                        <thead>
                            <h1 class="text-center">Biodata Diri</h1>
                        </thead>
                        <tbody class="rt1">
                        </tbody>
                        <tbody>
                            <tr>
                                <td>NPM</td>
                                <td>:</td>
                                <td>011220013</td>
                            </tr>
                            <tr>
                                <td>Nama</td>
                                <td>:</td>
                                <td>Ahmad Miftah Fadhli</td>
                                <tr>
                                    <td>Jenis Kelamin</td>
                                    <td>:</td>
                                    <td>Pria</td>
                                </tr>
                                <tr>
                                    <td>Prodi</td>
                                    <td>:</td>
                                    <td>Informatika</td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>:</td>
                                    <td>Jl.Sukabangun II </td>
                                </tr>
                                <tr>
                                    <td>Tempat Tanggal Lahir</td>
                                    <td>:</td>
                                    <td>Palembang, 4 Agustus 2004</td>
                                </tr>
                                <tr>
                                    <td>No HP</td>
                                    <td>:</td>
                                    <td>0895418679020</td>
                                </tr> 
                                <tr>
                                    <td>Hobi</td>
                                    <td>:</td>
                                    <td>
                                        <ul>
                                            <li>Game</li>
                                            <li>Memasak</li>
                                        </ul>
                                    </td>
                                </tr>    
                                <tr>
                                    <td>Riwayat Pendidikan</td>
                                    <td>:</td>
                                    <td>
                                        <ol>
                                            <li>SDN 134 Palembang </li>
                                            <li>SMPN 46 Palembang</li>
                                            <li>SMK Utama Bakti Palembang</li>
                                            
                                        </ol>
                                    </td>
                                </tr>    
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
            </div>
            Je m'appelle Ahmad. J'habite en indonesia  J'ai dix-neuf ans. J'aime joeur jeu video avec mon portable et j'aime manger mangue tous les jours
        </div>
       
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/ bootstrap.bundle.min.js"></script>
    
</body>
</html>

